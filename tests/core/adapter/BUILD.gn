# Copyright (c) 2022-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/test.gni")
import("../../../dsoftbus.gni")

module_output_path = "dsoftbus/core"
dsoftbus_root_path = "../../.."

ohos_unittest("LnnKVAdapterTest") {
  module_out_path = module_output_path
  sources = [ "unittest/lnn_kv_adapter_test.cpp" ]
  include_dirs = [
    "$dsoftbus_root_path/core/adapter/kv_store/include",
    "$dsoftbus_root_path/core/adapter/bus_center/include",
    "$dsoftbus_root_path/core/common/include",
  ]
  deps = [
    "$dsoftbus_root_path/core/common:softbus_utils",
    "$dsoftbus_root_path/core/frame:softbus_server",
  ]
  external_deps = [ "kv_store:distributeddata_inner" ]
}

ohos_unittest("LnnKVAdapterWrapperTest") {
  module_out_path = module_output_path
  sources = [ "unittest/lnn_kv_adapter_wrapper_test.cpp" ]
  include_dirs = [
    "$dsoftbus_root_path/core/adapter/kv_store/include",
    "$dsoftbus_root_path/core/adapter/bus_center/include",
    "$dsoftbus_root_path/core/common/include",
    "$dsoftbus_root_path/core/bus_center/lnn/net_builder/include",
    "$dsoftbus_root_path/core/bus_center/lnn/net_ledger/common/include",
    "$dsoftbus_root_path/core/bus_center/interface",
  ]
  deps = [
    "$dsoftbus_root_path/core/common:softbus_utils",
    "$dsoftbus_root_path/core/frame:softbus_server",
  ]
  external_deps = [ "kv_store:distributeddata_inner" ]
}

ohos_unittest("AdapterDsoftbusRsaCryptoTest") {
  module_out_path = module_output_path
  sources = [ "unittest/dsoftbus_rsa_crypto_test.cpp" ]

  include_dirs = [
    "$dsoftbus_root_path/adapter/common/include",
    "$dsoftbus_root_path/core/adapter/huks/include",
    "$dsoftbus_root_path/core/adapter/transmission/include",
    "$dsoftbus_root_path/core/common/include",
    "$dsoftbus_root_path/core/common/dfx/interface/include",
    "$dsoftbus_root_path/interfaces/kits/common",
    "//third_party/bounds_checking_function/include",
  ]

  deps = [
    "$dsoftbus_root_path/adapter:softbus_adapter",
    "$dsoftbus_root_path/core/common:softbus_utils",
    "$dsoftbus_root_path/core/frame:softbus_server",
    "//third_party/bounds_checking_function:libsec_static",
    "//third_party/googletest:gtest_main",
  ]

  if (is_standard_system) {
    deps += [ "//third_party/openssl:libcrypto_shared" ]
    external_deps = [
      "c_utils:utils",
      "hilog:libhilog",
      "huks:libhukssdk",
    ]
  } else {
    external_deps = [
      "c_utils:utils",
      "hilog:libhilog",
    ]
  }
}

group("unittest") {
  testonly = true
  deps = [ ":AdapterDsoftbusRsaCryptoTest" ]
  if (dsoftbus_feature_lnn_cloud_sync) {
    deps += [
      ":LnnKVAdapterTest",
      ":LnnKVAdapterWrapperTest",
    ]
  }
}

group("fuzztest") {
  testonly = true
  deps = [ "fuzztest:fuzztest" ]
}
